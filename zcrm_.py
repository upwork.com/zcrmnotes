import requests

client_id = '1000.7KIDZFIW10WT00600NKKOA7UIXVU6H'
client_secret = '7a709bb13b218bf94009d9cbbf8a61fd9bbfe12ec2'
refresh_token =  '1000.924e2fa4d9c4380af2e1a90bb51bcdde.6abaa0aabafd9f51de4139019fd086eb'
grant_token = '1000.75e857cfd1daf73e180b569d0440131c.0f4aa22f8e7efc614536e7e0415beb1a'
redirect_uri= 'https://dpprl27dxp.pagekite.me/'
api_auth = {'Authorization': 'Zoho-oauthtoken 1000.79f84ed38a828cc46b22bb6204ba219e.f0db0d43a46ecd2aee7e502c1685f5bc'}

base_url = 'https://accounts.zoho.com/oauth/v2/token'

params = {
    'grant_type': 'authorization_code',
    'client_id': client_id,
    'client_secret': client_secret,
    'redirect_uri': 'https://dpprl27dxp.pagekite.me/',
    'code': grant_token
}


def zohocrm_token_url():
    """Construct a valid url.
    
    For retrieving a refresh token
    """
    return (
        base_url +
        f'?refresh_token={refresh_token}&client_id={client_id}' +
        f'&client_secret={client_secret}&grant_type=refresh_token'
    )

def zohocrm_token_refresh():
    """Request a new access token.
    
    Sends a post request to accounts.zoho.com.
    On success; returns a new access token
    """
    return requests.post(zohocrm_token_url()).json()

def zohocrm_credentials(token):
#     token = zohocrm_token_refresh()
    return {
        'Authorization': f"Zoho-oauthtoken {token['access_token']}"
    }